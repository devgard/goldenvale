<?php
  $metaDescription="Kingdom of Goldenvale";
  $metaTitle = "Kingdom of Goldenvale";
?>

<meta name="description" content="{{$metaDescription}}" />
<meta name="title" content="{{$metaTitle}}" />

<meta property="og:url" content="http://www.goldenvale.org" />

<meta property="og:description" content="{{$metaDescription}}" />

<meta property="article:author" content="James Fefes" />
<meta property="article:publisher" content="http://www.goldenvale.org" />

<meta property="og:title" content="{{$metaTitle}}" />
<meta property="og:type" content="website" />
<meta property="og:image" content="/static/images/GVGryphon.jpg" />
<meta property="og:image:height" content="100" />
<meta property="og:image:type" content="image/png" />

<meta name="viewport" content="width=device-width">

<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico">
