@extends('layout.base')

<?php $title="Home" ?>

@section('content')


<!-- banner image -->
<div class="row">
  <div style="background-image:url('/images/banner.jpg');background-repeat:no-repeat;height:255px;background-position:center;"> </div>
</div>

<!-- Row 2 -->
<div class="row" style="margin-top:20px">
  <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 hidden-xs text-center">
      @include('partials.parks')
  </div>
  <div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1">
    <div class="hidden-lg hidden-md well">
      @include('partials.events')
    </div>


<!-- mobile Parks button -->
    <div class="visible-xs text-center">
      On mobile? <button type="button" class="btn btn-success" data-toggle="modal" data-target="#parks"> Click here to browse parks</button><br><br>
      <div class="modal fade" id="parks" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Parks in Goldenvale</h4>
            </div>
            <div class="modal-body">
              @include('partials.parks')
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- content -->
    <div class="row">
      <div class="visible-md well">
        @include('partials.events')
      </div>
    </div>

    <div class="row">
      <div class="well">
          <h3> Come play Amtgard! </h3>
          <p>Goldenvale's sponsored lands are listed on the left side of this page. The fastest way to find a nearby park is here: <a href="http://amtgard.com/ork/orkui/index.php?Route=Atlas">The Amtgard Atlas</a> </p>
      </div>
    </div>

  </div>

</div>
@stop
