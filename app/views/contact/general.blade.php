@extends('layout.base')

<?php $section="Contact"; $title="Contact"; ?>

@section('content')
  <section id="" class="container-fluid text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <p>
                  Use this form for any questions. Please be as thorough as possible.
                </p>
              </div>
            </div>

            <div class="well" id="form-well">
              <form class="form-horizontal" method="post" action="/contact/question">
                <div class="form-group">
                  <label for="player" class="col-sm-3 control-label">Your Name:</label>
                  <div class="col-sm-3">
                    <input type="name" class="form-control" name="player" placeholder="Your Name">
                  </div>
                  <label for="park" class="col-sm-2 control-label">Your Email:</label>
                  <div class="col-sm-3">
                    <input type="email" class="form-control" name="email" placeholder="Your Email">
                  </div>
                </div>

                <div class="form-group">
                  <label for="ork" class="control-label">If applicable, have you looked in RoP or Corpora as appropriate?</label>
                  <br>
                  <label class="radio-inline">
                    <input type="radio" name="local" id="inlineRadio1" value="NOT" checked> No
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="local" id="inlineRadio2" value="">Yes
                  </label>
                </div>

                <div class="form-group">
                  <label for="msg" class="col-sm-3 control-label">What can we help you with?</label>
                  <div class="col-xs-7">
                    <textarea class="form-control" name="msg" id="msg" rows="3"></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <h4>Send this recommendation to:</h4>
                  <input type="checkbox" name="sources[]" value="Monarch" checked><label>  &nbsp;Monarch</label> &nbsp; &nbsp;
                  <input type="checkbox" name="sources[]" value="Regent" checked><label>  &nbsp;Regent</label> &nbsp; &nbsp;
                  <input type="checkbox" name="sources[]" value="Prime_Minister" checked><label>  &nbsp;PM</label> &nbsp; &nbsp;
                  <input type="checkbox" name="sources[]" value="Champion" checked><label>  &nbsp;Champion</label> &nbsp; &nbsp;

                  <input type="checkbox" name="sources[]" value="GMR" checked><label>  &nbsp;GMR</label><br/>
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-primary" value="Submit">
                </div>
              </form>
            </div>
  </section>
@stop
