@extends('layout.base')
<?php $title="Contact" ?>

@section('content')

<h3>Reply to the following {{$contact->type}}: </h3>
		<div class="well">
					{{$contact->content}}
		</div>


		Your message for the monarch:
		<form class="" action="/reply-monarch" method="post">
			<div class="form-group" style="max-width:500px;">
				<textarea name="reply" rows="8" cols="40"></textarea>
			</div>

			<p>
			</p>

			<input type="hidden" name="id" value="{{$contact->id}}">
			<input type="hidden" name="source" value="{{$contact->source}}">
			<input type="submit" class="btn btn-info" value="Send">
		</form>
		<br>

@stop
