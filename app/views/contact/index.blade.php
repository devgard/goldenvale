@extends('layout.base')

<?php $section="Contact"; $title="Contact"; ?>

@section('content')

@if (Session::has('message'))
  <div class="alert alert-info text-center">
    Message received! <br>
    Please check your spam-box!
  </div>
@endif

  <div class="well row">
    <div class="col-sm-3 col-sm-offset-2">
      <a class="btn btn-primary" href="/contact/award">Award Recommendation</a> <br> <br>
      <a class="btn btn-primary" href="/contact/title">Title Recommendation</a> <br> <br>
    </div>

    <div class="col-sm-3">
      <a class="btn btn-primary" href="/contact/general">General Question</a> <br> <br>
      <a class="btn btn-primary" href="/contact/travel">Travel Invitation</a>
    </div>

    <div class="col-sm-3">
      <a class="btn btn-primary" href="/contact/issue">Player Issue</a> <br> <br>
    </div>
  </div>

<p class="text-center">
  Choose from the options below. <br>
  Don't see what you're looking for?
  <a target="_blank" href="https://www.facebook.com/groups/goldenvale/">Get in touch with us on facebook.</a>
</p>
@stop
