@extends('layout.base')

<?php $section="Contact"; $title="Contact"; ?>

@section('content')
  <section id="" class="container-fluid text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <p>
                  Use this form for an award recommendation. Please be as thorough as possible.
                </p>
              </div>
            </div>

            <div class="well" id="form-well">
              <form class="form-horizontal" method="post" action="/contact/award">
                <div class="form-group">
                  <div class="col-xs-6">
                    <label for="player" class="control-label">Player Name</label>
                      <input type="name" class="form-control" name="player" placeholder="Player Name">
                  </div>
                  <div class="col-xs-6">
                    <label for="park" class="control-label">Player's Park</label>
                      <input type="name" class="form-control" name="park" placeholder="Park Name">
                  </div>
                </div>

                <div class="form-group">
                  <label for="ork" class="col-sm-5 control-label">Please provide link to the player's ORK:</label>
                    <input type="name" class="form-control" name="ork" placeholder="ORK" style="max-width:400px">
                  </div>

                <div class="form-group">
                  <div class="row">
                    <label for="message" class="control-label">What award, and how have they earned it?</label>
                  </div>
                    <textarea class="form-control" name="msg" id="msg" rows="3"></textarea>
                </div>

                <div class="form-group">
                  <h4>Send this recommendation to:</h4>
                  <input type="checkbox" name="sources[]" value="Monarch" checked><label>  &nbsp;Monarch</label> &nbsp; &nbsp;

                  <input type="checkbox" name="sources[]" value="Regent" checked><label>  &nbsp;Regent</label><br/>
                </div>
              </div>

                </div>

                <div class="text-center">
                  <div class="container">
                    <p>
                    </p>
                  </div>

                    <div class="form-group">
                      <label for="email" class="col-sm-5 control-label">Your email:</label>
                        <input type="name" class="form-control" name="email" placeholder="Your email" style="max-width:400px">
                      </div>
                  <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Submit">
                  </div>
                </div>
              </form>
            </div>
  </section>
@stop
