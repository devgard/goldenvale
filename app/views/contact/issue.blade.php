@extends('layout.base')

<?php $section="Contact"; $title="Contact"; ?>

@section('content')
  <section id="" class="container-fluid text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <p>
                  Use this form for any issues you need to report. Please be as thorough as possible.
                </p>
              </div>
            </div>

            <div class="well" id="form-well">
              <form class="form-horizontal" method="post" action="/contact/issue">
                <div class="form-group">
                  <label for="player" class="col-sm-3 control-label">Player Name</label>
                  <div class="col-sm-3">
                    <input type="name" class="form-control" name="player" placeholder="Player Name">
                  </div>
                  <label for="park" class="col-sm-2 control-label">Player's Park</label>
                  <div class="col-sm-3">
                    <input type="name" class="form-control" name="park" placeholder="Park Name">
                  </div>
                </div>

                <div class="form-group">
                  <label for="ork" class="control-label">Does the local monarchy know of this?</label>
                  <br>
                  <label class="radio-inline">
                    <input type="radio" name="local" id="inlineRadio1" value="DOES NOT" checked> No
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="local" id="inlineRadio2" value="does">Yes
                  </label>
                </div>

                <div class="form-group">
                  <label for="msg" class="col-sm-3 control-label">What is the issue?</label>
                  <div class="col-xs-7">
                    <textarea class="form-control" name="msg" id="msg" rows="3"></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <h4>Send this recommendation to:</h4>
                  <input type="checkbox" name="sources[]" value="Monarch" checked><label>  &nbsp;Monarch</label> &nbsp; &nbsp;
                  <input type="checkbox" name="sources[]" value="Prime_Minister" checked><label>  &nbsp;PM</label> &nbsp; &nbsp;
                  <input type="checkbox" name="sources[]" value="Champion" checked><label>  &nbsp;Champion</label> &nbsp; &nbsp;

                  <input type="checkbox" name="sources[]" value="GMR" checked><label>  &nbsp;GMR</label><br/>
                </div>
              </div>

                <div class="text-center">
                  <div class="container">
                    <p>
                    </p>
                  </div>
                    <div class="form-group">
                      <label for="email" class="col-sm-5 control-label">Your email:</label>
                        <input type="name" class="form-control" name="email" placeholder="Your email" style="max-width:400px">
                      </div>
                  <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Submit">
                  </div>
              </form>
            </div>
  </section>
@stop
