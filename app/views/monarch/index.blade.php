@extends('layout.base')
<?php $title="Admin" ?>

@section('content')

<div class="well">
	<a href="/logout" class="btn btn-default">Logout</a>
</div>

@if(isset($contacts))
	@foreach($contacts as $contact)

		<div class="well">
			<div class="row">
				<div class="col-sm-2">
					{{$contact->type}}

				</div>
				<div class="col-sm-2">
					{{$contact->email}}

				</div>
				<div class="col-sm-4">
					{{$contact->content}}

				</div>
				<div class="col-sm-4">
					<form class="" action="/approve" method="post">
						<input type="hidden" name="id" value="{{$contact->id}}">
						<input type="hidden" name="source" value="{{$contact->source}}">
						<input type="submit" class="btn btn-success" name="Approve" value="Approve/Respond">
					</form>
					<br>



					<form class="" action="/deny" method="post">
						<input type="hidden" name="id" value="{{$contact->id}}">
						<input type="hidden" name="source" value="{{$contact->source}}">
						<input type="submit" class="btn btn-danger" name="Deny" value="Deny/Delete">
					</form>
					<br>

					<form class="" action="/reply" method="post">
						<input type="hidden" name="id" value="{{$contact->id}}">
						<input type="hidden" name="source" value="{{$contact->source}}">
						<input type="submit" class="btn btn-info" name="Reply" value="Reply">
					</form>

					<br>

				</div>
			</div>
		</div>

	@endforeach
@endif
<p class="text-center">
	* Replies recieved will still be anonymous
</p>

@stop
