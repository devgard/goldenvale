@extends('layout.base')
<?php $title="Admin" ?>

@section('content')

<h3>Viewing the following {{$contact->type}}: </h3>
		<div class="well">
					{{$contact->content}}
		</div>



		<form class="" action="/deleted" method="post">
			<div class="form-group" style="max-width:500px;">
				Enter a message/comment for the user who submitted:
				<input class="form-control" type="text" name="comment" value="" placeholder="Message">
			</div>

			<input type="hidden" name="id" value="{{$contact->id}}">
			<input type="hidden" name="source" value="Monarch">
			<input type="submit" class="btn btn-danger" value="Delete">
		</form>
		<br>

		<p>
			Clicking delete will send an email notification to the user who submitted the request.
		</p>


@stop
