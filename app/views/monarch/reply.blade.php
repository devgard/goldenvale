@extends('layout.base')
<?php $title="Admin" ?>

@section('content')

<h3>Reply to the following {{$contact->type}}: </h3>
		<div class="well">
					{{$contact->content}}
		</div>


		Your message:
		<form class="" action="/make-reply" method="post">
			<div class="form-group" style="max-width:500px;">
				<textarea name="reply" rows="8" cols="40"></textarea>
			</div>

			<input type="hidden" name="id" value="{{$contact->id}}">
			<input type="hidden" name="source" value="Monarch">
			<input type="submit" class="btn btn-info" value="Send">
		</form>
		<br>
@stop
