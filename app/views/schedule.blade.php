@extends('layout.base')
<?php $title="About/Contact" ?>

@section('content')

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="well text-center">
        <h3>Current kingdom schedule</h3>

        <p>
          <br><a href="https://www.facebook.com/groups/goldenvale"><strong> Stay up to date on our Facebook page!</strong></a><br><br>

          <a href="https://www.facebook.com/groups/goldenvale/permalink/1271100966237892/">Current reign schedle</a> <br><br><br><br>
          <p>
            <strong>Reign 42 schedule</strong> <br>
            3/19 - Althing <br>
            - Core Lands- Kingdom Monarch Travel Fun <br> <br>
            4/9 Voting Opens for April Althing <br> <br>
            4/16 Althing <br> <br>
            4/23 Prime Minister Declarations Due <br> <br>
            5/14 Voting Opens for May Althin <br> <br>
            5/21 Althing <br>
            Associated and Core Lands-Prime Minister Elections <br> <br>
            5/27-5/30 Great Eastern, Weaponmaster, Midreign Feas <br> <br>
            5/28 Crown Qual Location Bids Due <br> <br>
            6/11 Voting Open for June Althing <br> <br>
            6/18 Althing <br>
            ALL lands-Crown Qualification Locations Bids <br> <br>
            7/9 Voting Opens for July Althing <br>
            Great Eastern Bids Due <br> <br>
            7/16 Althing <br>
            Great Eastern Bids Presented <br> <br>
            7/23 Crown Declarations Due <br> <br>
            8/13 Kingdom Crown Quals, Warmaster <br>
            Voting Opens for August Althing <br> <br>
            8/20 Althing <br>
            1) Associated and Core lands- Crown Elections <br> <br>
            2)ALL lands-Great Eastern Bids Voted <br>
            9/2-9/5 Fury of the Northlands <br> <br>
            Coronation for Reign 43 <br>
          </p>


      </div>
  </div>
</div>
@stop
