<!doctype html>

<html>

<head>
  <title>Your message has been received.</title>
</head>
<body>
  <h3>Message received!</h3>
  <p>
    Your message submitted to <a href="http://goldenvale.org">goldenvale.org</a> has been received.
    <br>
    Once the monarchy sees your message, you will get a notification with their action.
    <br>
    <br>
    <br>
    Thank you for your time!
  </p>
  <br><br><br><br><br><strong>***This is an automated email, no one will respond***</strong>
  
</body>
</html>
