<!doctype html>

<html>

<head>
  <title>Award Recommendation Approved!</title>
</head>
<body>
  Action has been taken on the following item:<br>


  {{$contact->player}}<br>
  {{$contact->park}}<br><br>
  {{$contact->content}}<br>
<br><br>
  Your comments: {{$comment}}
  <br><br><br><br><br><strong>***This is an automated email, no one will respond***</strong>

</body>
</html>
