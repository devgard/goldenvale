<!doctype html>

<html>

<head>
  <title>New Issue Reported.</title>
</head>
<body>
  Regarding: {{$player}}, of {{ $park }} <br>
  <br>
  The local park {{$local}} know about this.
  <br>

  Message contents: {{$msg}}
  <br><br><br><br><br><strong>***This is an automated email, no one will respond***</strong>
  
</body>
</html>
