<!doctype html>

<html>

<head>
</head>
<body>
  A reply has been made on the following entry:<br>

  {{$contact->player}}<br>
  {{$contact->park}}<br><br>
  {{$contact->content}}<br>
  <hr>

  <strong>Player's comments</strong>: {{$reply}}
  <br><br>
  <br><br>
  To reply to this email, go to <a href="http://goldenvale.org/monarch">http://goldenvale.org/monarch</a>
</body>
</html>
