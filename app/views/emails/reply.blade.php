<!doctype html>

<html>

<head>
</head>
<body>
  A reply has been made on the following contact entry:<br>

  {{$contact->player}}<br>
  {{$contact->park}}<br><br>
  {{$contact->content}}<br>
  <hr>

  <strong>Monarch's comments</strong>: {{$reply}}
  <br><br>
  <br><br>
  To reply to this email, go to <a href="http://goldenvale.org/contact/{{$contact->id}}">http://goldenvale.org/contact/{{$source}}/{{$contact->id}}</a>
</body>
</html>
