@extends('layout.base')
<?php $title="About/Contact" ?>

@section('content')

<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="well text-center">
      <h3>Current officers</h3>
      <p>King: Nexus Crow<br>
          Regent: Dencri Blackwater <br>
          Prime Minister: Jondal'ar Silvertongue <br>
          Champion: Noodles Aldente <br>
        </p>
        <hr>
      </div>

      <div class="well text-center">
          <h3>Contact information</h3>
        <p>
          Contact the current officers at: <a href="mailto:monarchgv@gmail.com">monarchgv@gmail.com</a> <br>
          Contact the Webmaster at: <a href="mailto:webmaster@goldenvale.org">webmaster@goldenvale.org</a> <br>
        </p>
      </div>
  </div>


  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <h2 class="text-center"> News </h2>
    <br>
    <div class="well text-center">
        <h3> Forums! </h3>
        <p> Goldenvale Forums located at: <a href="http://goldenvale.freeforums.net/">http://goldenvale.freeforums.net/</a>
    </div>
  </div>
</div>

@stop
