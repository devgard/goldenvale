<?php

class Contact extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contacts';
	public $timestamps = false;
	protected $fillable = array('content', 'type');

	protected $hidden = array('email');

	public function Comment(){
		return $this->hasMany('Comment');
	}
}
