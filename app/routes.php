<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});

Route::get('/about', function()
{
	return View::make('about');
});

Route::get('/schedule', function()
{
	return View::make('schedule');
});

Route::get('/contact', function()
{
	return View::make('contact.index');
});

Route::get('/contact/award', 'ContactController@award');
Route::post('/contact/award', 'ContactController@postaward');

Route::get('/contact/title', 'ContactController@title');
Route::post('/contact/title', 'ContactController@posttitle');

Route::get('/contact/general', 'ContactController@general');
Route::post('/contact/question', 'ContactController@postgeneral');

Route::get('/contact/issue', 'ContactController@issue');
Route::post('/contact/issue', 'ContactController@postissue');

Route::get('/contact/travel', 'ContactController@travel');
Route::post('/contact/travel', 'ContactController@posttravel');


Route::get('/login', array('before' => array('force.ssl'), function() {
	return App::make('LoginController')->index();
}));

Route::post('/login', array('before' => array('csrf', 'force.ssl'), function() {
	return App::make('LoginController')->doLogin();
}));


Route::get('/contact/{source}/{id}', function($source, $id) {
	return App::make('ReplyController')->getReply($source, $id);
});

Route::group(array('before' => 'auth'), function()
{
	Route::get('/logout', array('before' => 'auth', function() {
	return App::make('LoginController')->doLogout();
}));

	Route::get('/dashboard', 'UserController@index');

	Route::post('/approve', 'ResponseController@index');
	Route::post('/approved', 'ResponseController@doApprove');

	Route::post('/deleted', 'ResponseController@doDelete');

	Route::post('/deny', 'ResponseController@deny');

	Route::post('/reply', 'ReplyController@index');
	Route::post('/make-reply', 'ReplyController@create');

	Route::post('/reply-monarch', 'ReplyController@doReply');

});
