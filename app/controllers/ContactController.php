<?php

class ContactController extends BaseController {

	public function award()
	{
		return View::make('contact.award');
	}

	public function postaward()
	{
		$input = Input::Get();
		$email = $input["email"];

		foreach ($input['sources'] as $source) {
			$contact = new Contact();
			$contact->email = $email;
			$contact->source = $source;
			$contact->content = "<strong>Player</strong> : " . $input['player'] . "<br><strong>Park</strong>: " . $input['park'] . "<br><br><strong>ORK</strong>: <a rel='NOFOLLOW' target='_blank' href=" . $input['ork'] . ">" . $input['ork']. "</a><br><br><strong>Message</strong>: " . $input['msg'];
			$contact->type = "Award Recommendation";
			$contact->save();

			$officer_email = User::where('type', $source)->first()->pluck('email');

			Mail::send('emails.award', $input, function($message) use ($officer_email)
			{
				$message->from('gvamtgard@gmail.com', 'Goldenvale Website');
				$message->to($officer_email)->subject('[GV Website] New Award Recommendation!');
			});
		}

		try {
			Mail::send('emails.confirmation', $input, function($message) use ($email)
					{
						$message->from('gvamtgard@gmail.com', 'Goldenvale Website');
						$message->to($email)->subject('[GV Website] Message received!');
					});

		} catch (Exception $e) {

		}



		Session::flash('message', 'true');
		return Redirect::to('/contact');

	}


	public function title()
	{
		return View::make('contact.title');
	}

	public function posttitle()
	{
		$input = Input::Get();


		$email = $input["email"];

		Mail::send('emails.confirmation', $input, function($message) use ($email)
				{
					$message->from('gvamtgard@gmail.com', 'Goldenvale Website');
					$message->to($email)->subject('[GV Website] Message received!');
				});

		foreach ($input['sources'] as $source) {
			$contact = new Contact();
			$contact->email = $email;
			$contact->source = $source;
			$contact->content = "<strong>Player</strong> : " . $input['player'] . "<br><strong>Park</strong>: " . $input['park'] . "<br><br><strong>ORK</strong>: <a rel='NOFOLLOW' target='_blank' href=" . $input['ork'] . ">" . $input['ork']. "</a><br><br><strong>Message</strong>:" . $input['msg'];
			$contact->type = "Award Recommendation";
			$contact->save();

			$officer_email = User::where('type', $source)->first()->pluck('email');

			Mail::send('emails.title', $input, function($message) use ($officer_email)
			{
				$message->from('gvamtgard@gmail.com', 'Goldenvale Website');
				$message->to($officer_email)->subject('[GV Website] New Title Recommendation!');
			});
		}



		Session::flash('message', 'true');
		return Redirect::to('/contact');
	}

	public function travel()
	{
		return View::make('contact.travel');
	}

	public function posttravel()
	{
		$input = Input::Get();

		$email = $input["email"];

		Mail::send('emails.confirmation', $input, function($message) use ($email)
		    {
		      $message->from('gvamtgard@gmail.com', 'Goldenvale Website');
		      $message->to($email)->subject('[GV Website] Message received!');
		    });

		foreach ($input['sources'] as $source) {
			$contact = new Contact();
			$contact->email = $email;
			$contact->source = $source;
			$contact->content = "<strong>Event</strong> : " . $input['event'] . "<br><strong>Park</strong>: " . $input['park'] . "<br><br><strong>Email</strong>: <a href=mailto:" . $input['email'] . ">" . $input['email']. "</a><br><br><strong>Message</strong>:" . $input['msg'];
			$contact->type = "General Question";
			$contact->save();

			$officer_email = User::where('type', $source)->first()->pluck('email');

			Mail::send('emails.event', $input, function($message) use ($officer_email)
			{
				$message->from('gvamtgard@gmail.com', 'Goldenvale Website');
				$message->to($officer_email)->subject('[GV Website] New Event Announcement!');
			});
		}

		Session::flash('message', 'true');
		return Redirect::to('/contact');
	}

	public function general()
	{
		return View::make('contact.general');
	}

	public function postgeneral()
	{
		$input = Input::Get();

		$email = $input["email"];

		Mail::send('emails.confirmation', $input, function($message) use ($email)
		    {
		      $message->from('gvamtgard@gmail.com', 'Goldenvale Website');
		      $message->to($email)->subject('[GV Website] Message received!');
		    });

		foreach ($input['sources'] as $source) {
			$contact = new Contact();
			$contact->email = $email;
			$contact->source = $source;
			$contact->content = "<strong>Player</strong> : " . $input['player'] . "<br>Player did: " . $input['local'] ." read Corpora/RoP". "<br><br><strong>Email</strong>: <a href=mailto:" . $input['email'] . ">" . $input['email']. "</a><br><br><strong>Message</strong>:" . $input['msg'];
			$contact->type = "General Question";
			$contact->save();

			$officer_email = User::where('type', $source)->first()->pluck('email');

			Mail::send('emails.question', $input, function($message) use ($officer_email)
			{
				$message->from('gvamtgard@gmail.com', 'Goldenvale Website');
				$message->to($officer_email)->subject('[GV Website] New Question Asked!');
			});
		}

		Session::flash('message', 'true');
		return Redirect::to('/contact');
	}


	public function issue()
	{
		return View::make('contact.issue');
	}

	public function postissue()
	{
		$input = Input::Get();

		$email = $input["email"];

		Mail::send('emails.confirmation', $input, function($message) use ($email)
		    {
		      $message->from('gvamtgard@gmail.com', 'Goldenvale Website');
		      $message->to($email)->subject('[GV Website] Message received!');
		    });

		foreach ($input['sources'] as $source) {
			$contact = new Contact();
			$contact->email = $email;
			$contact->source = $source;
			$contact->content = "<strong>Player</strong> : " . $input['player'] . "<br><strong>Park</strong>: " . $input['park'] . "<br><br><strong>Message</strong>:" . $input['msg'];
			$contact->type = "General Question";
			$contact->save();

			$officer_email = User::where('type', $source)->first()->pluck('email');

			Mail::send('emails.issue', $input, function($message) use ($officer_email)
			{
				$message->from('gvamtgard@gmail.com', 'Goldenvale Website');
				$message->to($officer_email)->subject('[GV Website] New Issue Reported');
			});
		}

		Session::flash('message', 'true');
		return Redirect::to('/contact');
	}




}
