<?php

class ReplyController extends BaseController {

  public function index(){
    $input = Input::get();
    $contact = Contact::where('id', $input['id'])->first();

    return View::make('monarch.reply', array('contact' => $contact));
  }

  public function create(){
    $input = Input::get();

    $contact = Contact::where('id', $input['id'])->first();
    $input['contact'] = $contact;

    $email = $contact->email;

    /*
    $comment = new Comment();
    $comment->contact_id = $input['id'];
    $comment->content = "Your reply: " .$input['reply'];
    $comment->save();
    */


    Mail::send('emails.reply', $input, function($message) use ($email)
        {
          $message->from('gvamtgard@gmail.com', 'Goldenvale Website');
          $message->to($email)->subject('[GV Website] Reply requested');
        });

    Session::flash('message', 'true');
    return Redirect::to('/contact');
  }

  public function getReply($source, $id){
    $contact = Contact::where('id', $id)->first();

    return View::make('contact.reply', array('contact' => $contact, 'source' => $source));
  }

  public function doReply(){
    $input = Input::get();

    $contact = Contact::where('id', $input['id'])->first();
    $input['contact'] = $contact;

    $email = $contact->email;

    //create comment

    $officer_email = User::where('type', $input['source'])->first()->pluck('email');

    Mail::send('emails.send-reply', $input, function($message) use ($officer_email)
        {
          $message->from('gvamtgard@gmail.com', 'Goldenvale Website');
          $message->to($officer_email)->subject('[GV Website] Reply received');
        });

    Session::flash('message', 'true');
    return Redirect::to('/contact');
  }



}
