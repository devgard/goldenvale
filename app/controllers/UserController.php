<?php

class UserController extends BaseController {

	public function index()
	{
		$user = Auth::user();

		$contacts = Contact::where('source', $user->type)->get();

		return View::make('monarch.index', array('contacts' => $contacts));
	}

}
