<?php

class ResponseController extends BaseController {

  public function index(){
    $input = Input::get();
    $contact = Contact::where('id', $input['id'])->first();

    return View::make('monarch.response', array('contact' => $contact));
  }

  public function deny(){
    $input = Input::get();
    $contact = Contact::where('id', $input['id'])->first();

    return View::make('monarch.deny', array('contact' => $contact));
  }

  public function doApprove(){
    $input = Input::Get();

    $input['contact'] = Contact::where('id', $input['id'])->first();

    $email = $input['contact']->email;

    Mail::send('emails.approved', $input, function($message) use ($email)
        {
          $message->from('gvamtgard@gmail.com', 'Goldenvale Website');
          $message->to($email)->subject('[GV Website] New reply');
        });

    $officer_email = User::where('type', $input['source'])->first()->pluck('email');

    Mail::send('emails.reminder', $input, function($message) use ($officer_email)
    {
      $message->from('gvamtgard@gmail.com', 'Goldenvale Website');
      $message->to($officer_email)->subject('[GV Website] New reminder!');
    });

    $input['contact']->delete();

    return Redirect::to('/dashboard');
  }

  public function doDelete(){
    $input = Input::Get();

    $input['contact'] = Contact::where('id', $input['id'])->first();

    $email = $input['contact']->email;

    try {
      Mail::send('emails.deleted', $input, function($message) use ($email)
        {
          $message->from('gvamtgard@gmail.com', 'Goldenvale Website');
          $message->to($email)->subject('[GV Website] Request deleted');
        });

    } catch (Exception $e) {

    }


    $officer_email = Auth::user()->email;

    Mail::send('emails.reminder', $input, function($message) use ($officer_email)
      {
        $message->from('gvamtgard@gmail.com', 'Goldenvale Website');
        $message->to($officer_email)->subject('[GV Website] New reminder!');
      });

    $input['contact']->delete();

    return Redirect::to('/dashboard');
  }


}
