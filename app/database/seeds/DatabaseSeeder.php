<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
		$this->command->info('User table seeded');

	}
}

class UserTableSeeder extends Seeder {
    public function run()
    {
			$monarch = new User;
			$monarch->username = 'monarch';
			$monarch->type = 'Monarch';
			$monarch->email = 'monarchgv@gmail.com';
			$monarch->password = Hash::make('gv2016');
			$monarch->save();

			$regent = new User;
			$regent->username = 'regent';
			$regent->type = 'Regent';
			$regent->email = 'Firey.eyed@gmail.com';
			$regent->password = Hash::make('kingdomregent');
			$regent->save();

			$pm = new User;
			$pm->username = 'pm';
			$pm->type = 'Prime_Minister';
			$pm->email = 'Thescruffypuppy@gmail.com';
			$pm->password = Hash::make('pm2016');
			$pm->save();

			$champ = new User;
			$champ->username = 'champion';
			$champ->type = 'Champion';
			$champ->email = 'paulwanttaco@gmail.com';
			$champ->password = Hash::make('borealdale2016');
			$champ->save();

			$gmr = new User;
			$gmr->username = 'gmr';
			$gmr->type = 'GMR';
			$gmr->email = 'controldeleters@gmail.com';
			$gmr->password = Hash::make('floatingcrown');
			$gmr->save();
		}
}
